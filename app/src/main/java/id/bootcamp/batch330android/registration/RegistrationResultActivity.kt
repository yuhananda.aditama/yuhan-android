package id.bootcamp.batch330android.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        //Ambil data yang dikirim dari adctivity sebelumnya
        val firstName = intent.extras?.getString("keyFirstName")
        val lastName = intent.extras?.getString("keyLastName")
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val gender = intent.extras?.getString("keyGender")
        val languange = intent.extras?.getStringArrayList("keyLanguage")
        val state = intent.extras?.getString("keyState")

        //Ambil Objek dari layout
        val textFirstName = findViewById<TextView>(R.id.textFirstName)
        val textLastName = findViewById<TextView>(R.id.textLastName)
        val textAddress = findViewById<TextView>(R.id.textAddress)
        val textEmail = findViewById<TextView>(R.id.textEmail)
        val textGender = findViewById<TextView>(R.id.textGender)
        val textLanguage = findViewById<TextView>(R.id.textLanguage)
        val textState = findViewById<TextView>(R.id.textState)

        //Set text data dari activity sebelumnya
        textFirstName.text = "First Name : $firstName"
        textLastName.text = "Last Name : $lastName"
        textAddress.text = "Address : $address"
        textEmail.text = "Email : $email"
        textGender.text = "Gender : $gender"
        textLanguage.text = "Language : $languange"
        textState.text = "state : $state"



    }
}