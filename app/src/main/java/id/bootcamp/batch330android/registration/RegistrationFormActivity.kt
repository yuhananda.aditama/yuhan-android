package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import id.bootcamp.batch330android.R
import id.bootcamp.batch330android.utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Perintah memasang layout
        setContentView(R.layout.activity_registration_form)

        //Ambil semua object yang ada di form
        val editTextFirstName = findViewById<EditText>(R.id.etFirstName)
        val editTextLastName = findViewById<EditText>(R.id.etLastName)
        val radioMale = findViewById<RadioButton>(R.id.radioMale)
        val radioFemale = findViewById<RadioButton>(R.id.radioFemale)
        val checkIndonesia = findViewById<CheckBox>(R.id.cbIndonesia)
        val checkEnglish = findViewById<CheckBox>(R.id.cbEnglish)
        val editTextEmail = findViewById<EditText>(R.id.etEmail)
        val editTextAddress = findViewById<EditText>(R.id.etAddress)
        val spinState = findViewById<Spinner>(R.id.spinnerState)
        val btnSubmit = findViewById<Button>(R.id.btnSubmit)

        //Ambil array string dari resource
        val states = resources.getStringArray(R.array.states)

        //Mengisi Spinner/Dropdown
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1,
            states)
        spinState.adapter = adapter

        //Buat clickListener button submit
        btnSubmit.setOnClickListener {
            //Ambil value dari form
            //EditText
            val firstName = editTextFirstName.text.toString()
            val lastName = editTextLastName.text.toString()
            val email = editTextEmail.text.toString()
            val address = editTextAddress.text.toString()

            //Radio Button
            var gender = ""
            if (radioMale.isChecked){
                gender = "Male"
            } else if (radioFemale.isChecked){
                gender = "Female"
            }

            //Checkbox
            //Perintah deklarasi membuat list kosong
            val listLanguage = ArrayList<String>()
            if (checkIndonesia.isChecked){
                //Perintah buat menambahkan data di listLanguage
                listLanguage.add("Indonesia")
            }
            if (checkEnglish.isChecked){
                listLanguage.add("English")
            }

            //Spinner
            val state = spinState.selectedItem.toString()

            //Logic Validation
            //First Name tidak boleh kosong
            if (firstName.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "First Name tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //Email harus valid
            val isEmailValid = isValidEmail(email)
            if (isEmailValid == false){
                Toast.makeText(this@RegistrationFormActivity,
                    "Email tidak valid",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //Address tidak boleh kosong
            if (address.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Address tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //Gender harus dipilih
            if (gender.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Gender harus dipilih",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //Language harus dipilih minimal 1
            if (listLanguage.isEmpty()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Language harus pilih minimal 1",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val intent = Intent(this,RegistrationResultActivity::class.java)
            intent.putExtra("keyFirstName",firstName)
            intent.putExtra("keyLastName",lastName)
            intent.putExtra("keyEmail",email)
            intent.putExtra("keyAddress",address)
            intent.putExtra("keyGender",gender)
            intent.putExtra("keyLanguage",listLanguage)
            intent.putExtra("keyState",state)
            startActivity(intent)
        }






    }
}