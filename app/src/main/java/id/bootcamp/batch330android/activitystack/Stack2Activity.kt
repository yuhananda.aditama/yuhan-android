package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class Stack2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack2)

        //Ambil object button
        val btnGoMainActivity = findViewById<Button>(R.id.btnGoMainMenu)

        //Atur button setelah di click
        btnGoMainActivity.setOnClickListener {
            //Buat object Intent
            //Untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, MainActivity::class.java)
            //Fungsi tambahan untuk clear semua stack sebelum pindah ke MainActivity
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
}